<?php
require_once 'vendors/autoload.php';

use Romain\MeteoAlert\Source;
use Romain\MeteoAlert\Stat;

$source = new Source\MeteoFranceForecast();
$source->init(array(
	'url' => 'http://www.meteofrance.com/previsions-meteo-france/saint-clement-de-la-place/49370',
));
$source->load();

foreach($source as $period => $forecast) {

	echo '> '.$period."\n";
	$stats = array('frost-probability', 'temperature-min', 'wind-speed-max');
	foreach($stats as $stat) {
		if($forecast->available($stat)) {
			echo "\t- ".$stat." : ".$forecast->get($stat)->value()."\n";
		}
	}

//	if($forecast->available('frost-probability') && $forecast->get('frost-probability')->worseThan(new Stat\FrostProbability(0))) {
//		exit('frost-probability : '.$period.'=>'.$forecast->get('frost-probability')->value());
//	}
//	if($forecast->available('temperature-min') && $forecast->get('temperature-min')->worseThan(new Stat\TemperatureMin(3))) {
//		exit('temperature-min : '.$period.'=>'.$forecast->get('temperature-min')->value());
//	}
}