<?php
require_once 'vendors/autoload.php';

use Romain\MeteoAlert\Source;
use Romain\MeteoAlert\Stat;

$source = new Source\MeteoramaForecast();
$source->init(array(
	'url' => 'http://www.meteorama.fr/m%C3%A9t%C3%A9o-saint-cl%C3%A9ment-de-la-place.html?v=heure-par-heure',
));
$source->load();

var_dump($source->toArray());
