<?php
require_once 'vendors/autoload.php';

use Romain\MeteoAlert\Source;
use Romain\MeteoAlert\Stat;
use Romain\MeteoAlert\Alert;
use Romain\MeteoAlert\Tools;

$dependancies = array(
	'sources' => array(
		array(
			'source' => new Source\MeteoFranceForecast(),
			'dependancies' => array(
				'url' => 'http://www.meteofrance.com/previsions-meteo-france/saint-clement-de-la-place/49370',
			)
		),
		array(
			'source' => new Source\MeteoramaForecast,
			'dependancies' => array(
				'url' => 'http://www.meteorama.fr/m%C3%A9t%C3%A9o-saint-cl%C3%A9ment-de-la-place.html?v=heure-par-heure',
			)
		),
	),
);

$multi = new Source\Multi;
$multi->init($dependancies);
$multi->load();

foreach($multi as $period => $forecast) {
	$text = '';
	$text .= $period.', '.$forecast->get('summary')->formated()." : \n";
	$stats = array(
		'temperature' => array(
			'type' => 'list',
			'separator' => '-',
			'label' => 'Température',
			'stats' => array(
				'temperature-min',
				'temperature-max',
			),
		),
		'humidity' => array(
			'type' => 'list',
			'separator' => '-',
			'label' => 'Humidité',
			'stats' => array(
				'humidity-min',
				'humidity-max',
			),
		),
		'cloudiness' => array(
			'type' => 'list',
			'separator' => '-',
			'label' => 'Nuages',
			'stats' => array(
				'cloudiness-min',
				'cloudiness-max',
			),
		),
		'frost-probability' => 'Gel',
		'wind-speed-max' => array(
			'type' => 'list',
			'separator' => ', ',
			'label' => 'Vent',
			'stats' => array(
				'wind-speed-max',
				'wind-direction',
			),
		),
		'rain-probability' => array(
			'type' => 'list',
			'separator' => ', ',
			'label' => 'Pluie',
			'stats' => array(
				'rain-probability',
				'rainfall',
			),
		),
	);
	foreach($stats as $stat => $conf) {
		// reformattage
		if(is_int($stat) && is_string($conf)) {
			$stat = $conf;
		}
		if(is_string($conf)) {
			$conf = array('label' => $conf);
		}
		$conf += array(
			'type' => 'simple',
			'separator' => null,
			'label' => null,
			'stats' => $stat,
			'suffix' => null,
		);

		$stat_text = '';
		switch ($conf['type']) {
			case 'simple':
				if($forecast->available($conf['stats'])) {
					$stat_text = $forecast->get($conf['stats'])->formated();
				}
				break;
			case 'list':
				$stats_values = array();
				foreach((array)$conf['stats'] as $stat) {
					if($forecast->available($stat)) {
						$stats_values[] = $forecast->get($stat)->formated();
					}
				}
				$stat_text = implode($conf['separator'], array_unique($stats_values));
				break;
		}
		if(!empty($stat_text)) {
			$text .= "- ".$conf['label'].' : '.$stat_text.$conf['suffix']."\n";
		}
	}

	echo $text;

//$sms = new Tools\FreeMobileSmsClient();
//require_once 'config.php';
//$sms->add($sms_user, $sms_user_key);
//$sms->sendAll($text);
}












exit;

foreach($multi as $period => $forecast) {
	$text = '';
	$text .= $period." :\n";
	$stats = array(
		'summary' => '',
		'temperature-min' => 'T>', 
		'temperature-max' => 'T<', 
		'frost-probability' => 'Gel', 
		'wind-speed-max' => 'Vent',
		'rain-probability' => 'Pluie',
		'humidity-min' => 'Hum.>',
		'humidity-max' => 'Hum.<',
		'cloudiness-max' => 'Nuages<',
		'cloudiness-min' => 'Nuages>'
	);
	foreach($stats as $stat => $label) {
		if($forecast->available($stat)) {
			switch ($stat) {
				case 'summary':
					$text .= "- ".$forecast->get($stat)->value()."\n";
					break;
				case 'temperature-min':
				case 'temperature-max':
					$text .= "- ".$label." : ".$forecast->get($stat)->value()."°\n";
					break;
				case 'wind-speed-max':
				case 'wind-speed-max':
					$text .= "- ".$label." : ".$forecast->get($stat)->value()."km/h\n";
					break;
				default:
					$text .= "- ".$label." : ".$forecast->get($stat)->value()."%\n";
					break;
			}
			
		}
	}
	
	echo $text;

//$sms = new Tools\FreeMobileSmsClient();
//require_once 'config.php';
//$sms->add($sms_user, $sms_user_key);
//$sms->sendAll($text);
}