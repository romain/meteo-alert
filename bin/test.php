<?php
require_once 'vendors/autoload.php';

use Romain\MeteoAlert\Source;
use Romain\MeteoAlert\Stat;

// mini tests


// max (propbability)
$min0 = new Stat\FrostProbability(10);
$min1 = new Stat\FrostProbability(50);
$min2 = new Stat\FrostProbability(0);

if(!$min0->worseThan($min2)) {
	throw new Exception();
}
if($min0->worseThan($min1)) {
	throw new Exception();
}
if(!$min1->worseThan($min2)) {
	throw new Exception();
}

// min (temperature)
$min_t_0 = new Stat\TemperatureMin(5);
$min_t_1 = new Stat\TemperatureMin(10);
$min_t_2 = new Stat\TemperatureMin(-2);
if($min_t_0->worseThan($min_t_2)) {
	throw new Exception();
}
if(!$min_t_0->worseThan($min_t_1)) {
	throw new Exception();
}
if($min_t_1->worseThan($min_t_2)) {
	throw new Exception();
}