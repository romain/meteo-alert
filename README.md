Récupère certaines informations intéressantes pour le lendemain et surlendemain :
- température mini attendue
- risque de gelée
- risque de pluie
- précipitation attendue
- vent max

Sources :
- meteorama
- meteofrance
- meteociel ? http://www.meteociel.fr/previsions-wrf-1h/17114/saint_clement_de_la_place.htm

Statistiques :
- temparatures min/max
- probabilité de pluie
- pluviométrie
- himidité min/max
- probabilité d'orage
- probabilité de gel
- direction du vent
- vitesse du vent min/max
- couverture nuageuse
- octa ?