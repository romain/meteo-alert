<?php

namespace Romain\MeteoAlert\Tools;

/**
 * Singleton trait
 */
trait SingletonTrait {
	
	protected static $_instance = null;
	
	/**
	 * Get a default instance
	 * @return 
	 */
	public static function instance() {
		if (self::$_instance === null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}