<?php

namespace Romain\MeteoAlert\Tools;

/**
 * Mini client FreeMobile SMS api
 */
Class FreeMobileSmsClient {
	use SingletonTrait;

	protected $_config = array(
		'url' => 'https://smsapi.free-mobile.fr/sendmsg',
	);

	/**
	 * 
	 * @var list of api keys that will receive messages
	 */
	protected $_to = array();

	/**
	 *
	 * @var ressource
	 */
	protected $_curl = null;

	public function __construct($config = array()) {
		$this->_config = $config + $this->_config;
		$this->initialize();
		$this->reset();
	}

	public function __destruct() {
		curl_close($this->_curl);
	}

	/**
	 * Init client
	 */
	public function initialize() {
		$this->_curl = curl_init();
		curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, false);
	}

	/**
	 * Reset list of api keys
	 */
	public function reset() {
		$this->_to = array();
	}

	public function add($user, $key) {
		$this->_to[$user] = $key;
	}

	public function remove($user) {
		unset($this->_to[$user]);
	}

	public function to() {
		return $this->_to;
	}

	/**
	 * Send a message to each users
	 * @param type $message
	 */
	public function sendAll($msg) {
		foreach ($this->_to as $user => $key) {
			$this->send($user, $key, $msg);
		}
		return true;
	}

	/**
	 * Send a message to a user
	 * @param type $user
	 * @param type $key
	 * @param type $msg
	 * @return \Lebonrayon\Elephant\FreeSmsClient
	 * @throws \Exception
	 */
	public function send($user, $key, $msg) {
		curl_setopt(
			$this->_curl,
			CURLOPT_URL,
			$this->_config['url']."?user=".$user."&pass=".$key."&msg=" . urlencode($msg));
		
		curl_exec($this->_curl);
		
		if (200 != $code = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE)) {
			switch ($code) {
				case 400: $message = 'Un des paramètres obligatoires est manquant.';
					break;
				case 402: $message = 'Trop de SMS ont été envoyés en trop peu de temps.';
					break;
				case 403: $message = 'Vous n\'avez pas activé la notification SMS dans votre espace abonné Free Mobile ou votre identifiant/clé est incorrect.';
					break;
				case 500: $message = 'erreur sur serveur Free Mobile.';
					break;
				default: $message = 'erreur inconnue (code='.$code.')';
			}
			throw new \Exception($message, $code);
		}
		return true;
	}

}