<?php

namespace Romain\MeteoAlert\Stat;

class HailStormProbability extends Base {
	use StatPercentValueTrait, StatMaxTrait;
}