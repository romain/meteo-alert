<?php

namespace Romain\MeteoAlert\Stat;

class TemperatureMin extends Base{
	use StatIntTrait, StatMinTrait;

	public function unite() {
		return '°C';
	}
}