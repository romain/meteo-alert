<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Cloud cover
 */
class CloudinessMin extends Base {
	use StatPercentValueTrait, StatMinTrait;
}