<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Cloud cover
 */
class CloudinessMax extends Base {
	use StatPercentValueTrait, StatMaxTrait;
}