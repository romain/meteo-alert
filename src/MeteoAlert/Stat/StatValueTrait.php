<?php

namespace Romain\MeteoAlert\Stat;

trait StatValueTrait {
	protected $_value;
	
	public function set($value) {
		$this->_value = $this->cast($value);
	}
	
	public function value() {
		return $this->_value;
	}
	
	abstract public function cast($value);
}