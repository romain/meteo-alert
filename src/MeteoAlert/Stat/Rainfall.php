<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Average rainfall
 */
class Rainfall extends Base {
	use StatFloatTrait, StatMaxTrait;

	public function unite() {
		return 'ml';
	}
}