<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Humidity
 */
class HumidityMax extends Base {
	use StatPercentValueTrait, StatMaxTrait;
}