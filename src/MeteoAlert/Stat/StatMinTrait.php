<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Low is worst (min temperature)
 */
trait StatMinTrait {
	public function worseThan(StatInterface $to) {

		// no value, no worse
		if($this->value() === null) {
			return false;
		}
		// no $to value, worse
		if($to->value() === null) {
			return true;
		}

		return (int)$this->value() < (int)$to->value();
	}
	
	abstract public function value();
}