<?php

namespace Romain\MeteoAlert\Stat;

/**
 * High is worst (wind speed)
 */
trait StatMaxTrait {
	
	public function worseThan(StatInterface $to) {

		// no value, no worse
		if($this->value() === null) {
			return false;
		}

		// no $to value, worse
		if($to->value() === null) {
			return true;
		}
		
		return $this->cast($this->value()) > $to->cast($this->value());
	}
	
	abstract public function value();
	abstract public function cast($value);
}