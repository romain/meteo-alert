<?php

namespace Romain\MeteoAlert\Stat;

/**
 * No string comparison
 */
trait StatNoComparisonTrait {
	public function worseThan(StatInterface $to) {
		return false;
	}
	abstract public function value();
}