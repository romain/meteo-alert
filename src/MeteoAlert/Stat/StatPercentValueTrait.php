<?php

namespace Romain\MeteoAlert\Stat;

trait StatPercentValueTrait {
	public function cast($value) {
		if($value === null) {
			return null;
		}
		$cast = (int)$value;
		return max(0, min($cast, 100));
	}

	public function formated() {
		return $this->value().'%';
	}

	/**
	 * @return string
	 */
	abstract public function value();
}