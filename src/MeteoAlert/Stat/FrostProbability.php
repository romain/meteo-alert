<?php

namespace Romain\MeteoAlert\Stat;

class FrostProbability extends Base {
	use StatPercentValueTrait, StatMaxTrait;
}