<?php

namespace Romain\MeteoAlert\Stat;

class WindSpeedMax extends Base {
	use StatIntTrait, StatMaxTrait;

	public function unite() {
		return 'km/h';
	}
	
}