<?php

namespace Romain\MeteoAlert\Stat;

trait StatStringValueTrait {
	
	public function cast($value) {
		if($value === null) {
			return null;
		}
		return (string)$value;
	}

	public function formated() {
		return $this->value();
	}

	abstract public function value();
}