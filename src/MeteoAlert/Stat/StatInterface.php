<?php

namespace Romain\MeteoAlert\Stat;

interface StatInterface {
	
	/**
	 * Compare current stat to another
	 * @param \Romain\MeteoAlert\Stat\StatInterface $to
	 * @return boolean true if the current stats is "worse" than the other (null if not available)
	 */
	public function worseThan(StatInterface $to);
	
	/**
	 * Return stat value
	 */
	public function value();
	
	/**
	 * Set stat value
	 * @param type $value
	 */
	public function set($value);

	/**
	 * @return string
	 */
	public function formated();
}