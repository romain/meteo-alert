<?php

namespace Romain\MeteoAlert\Stat;

class TemperatureMax extends Base{
	use StatIntTrait, StatMaxTrait;

	public function unite() {
		return '°C';
	}
}