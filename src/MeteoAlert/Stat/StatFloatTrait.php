<?php

namespace Romain\MeteoAlert\Stat;

trait StatFloatTrait {
	
	public function cast($value) {
		if($value === null) {
			return null;
		}
		return (float) $value;
	}

	public function formated() {
		return $this->value().$this->unite();
	}

	/**
	 * @return string
	 */
	abstract public function unite();
}