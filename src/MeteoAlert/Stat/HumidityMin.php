<?php

namespace Romain\MeteoAlert\Stat;

/**
 * Humidity min
 */
class HumidityMin extends Base {
	use StatPercentValueTrait, StatMinTrait;
}