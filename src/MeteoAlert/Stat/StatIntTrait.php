<?php

namespace Romain\MeteoAlert\Stat;

trait StatIntTrait {
	
	public function cast($value) {
		if($value === null) {
			return null;
		}
		return (int) $value;
	}

	public function formated() {
		return $this->value().$this->unite();
	}

	/**
	 * @return string
	 */
	abstract public function unite();
}