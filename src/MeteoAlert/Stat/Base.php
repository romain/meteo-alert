<?php

namespace Romain\MeteoAlert\Stat;

/**
 * A base exemple for stats
 */
abstract class Base implements StatInterface {
	use StatValueTrait;
	
	public function __construct($value) {
		$this->set($value);
	}
}