<?php

namespace Romain\MeteoAlert\Source;

interface SourceInterface {
	
	/**
	 * Init required dependancies
	 * @param type $dependancies
	 */
	public function init($dependancies);
	
	/**
	 * Load source datas
	 */
	public function load();
	
	/**
	 * Return all stats
	 * @return array
	 */
	public function getForecasts();
	
	/**
	 * Get a forecast
	 * @return Forecast
	 */
	public function getForecast($period);
	
	/**
	 * Period available
	 */
	public function available($period);
	
}

