<?php

namespace Romain\MeteoAlert\Source;
use Romain\MeteoAlert\Stat\StatInterface;
use Romain\MeteoAlert\Tools;


/**
 * Store some forecast stats for a period
 */
class Forecast implements Tools\ToArrayInterface, \IteratorAggregate, \Countable/*, \ArrayAccess*/ {

	/**
     * Data
     *
     * @var array
     * @access private
     */
    private $stats = [];
	
	private $period = null;
	
	public function __construct($period = null, array $stats = array()) {
		$this->period = $period;
		foreach($stats as $name => $stat) {
			$this->set($name, $stat);
		}
	}
	
	public function period() {
		return $this->period;
	}
	
	public function available($name) {
		return array_key_exists($name, $this->stats);
	}
	
	public function set($name, StatInterface $stat) {
		$this->stats[$name] = $stat;
	}

	/**
	 * Set a stat value if worse than current forcast stat
	 * @param type $name
	 * @param StatInterface $stat
	 */
	public function setIfWorse($name, StatInterface $stat) {
		if(
			!$this->available($name) ||
			$stat->worseThan($this->get($name))
		) {
			$this->set($name, $stat);
		}
	}
	
	/**
	 * 
	 * @param type $name
	 * @return StatInterface
	 */
	public function get($name) {
		return $this->stats[$name];
	}
	
	public function getIterator() {
		return new \ArrayIterator($this->stats);
	}

	public function toArray() {
		//return get_object_vars($this);
		$array = array('period' => $this->period);
		foreach($this as $name => $stat) {
			$array['stats'][$name] = $stat->value();
		}
		return $array;
	}

	public function count($mode = 'COUNT_NORMAL') {
		return count($this->stats, $mode);
	}
}

