<?php

namespace Romain\MeteoAlert\Source;


/**
 * Forecast trait for sources
 */
trait ForecastsTrait {

	/**
	 *
	 * @var array Forecast
	 */
	protected $_forecasts = array();
	
	public function addForecast(Forecast $forecast) {
		$this->_forecasts[$forecast->period()] = $forecast;
	}
	
	/**
	 * Return a period forecast
	 * @param type $period
	 * @return type
	 */
	public function getForecast($period) {
		if(!empty($this->_forecasts)) {
			return $this->_forecasts[$period];
		}
		return null;
	}

	/**
	 * Return all forecasts
	 * @return type
	 */
	public function getForecasts() {
		return $this->_forecasts;
	}
	
	/**
	 * 
	 * @param type $period
	 * @return type
	 */
	public function available($period) {
		return array_key_exists($period, $this->_forecasts);
	}

	/**
	 * Forecasts iterator
	 * @return \ArrayIterator
	 */
	public function getIterator() {
		return new \ArrayIterator($this->_forecasts);
	}

	/**
	 * 
	 * @param type $mode
	 * @return type
	 */
	public function count($mode = 'COUNT_NORMAL') {
		return count($this->data, $mode);
	}

	/**
	 * 
	 * @return type
	 */
	public function toArray() {
		$array = array();
		foreach($this->_forecasts as $period => $forecast) {
			$array[$period] = $forecast->toArray();
		}
		return $array;
	}
}