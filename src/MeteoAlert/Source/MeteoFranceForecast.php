<?php

namespace Romain\MeteoAlert\Source;

use Romain\MeteoAlert\Stat;
use PHPHtmlParser\Dom;

/**
 * @Todo synthèse des infos en détail et extra détail pour récupérer les infos les "pires"
 * @TODO passage des dépendances
 */
class MeteoFranceForecast extends Base implements \IteratorAggregate {
	use TextTrait, ForecastsTrait;
	
	/**
	 * Forecasts for each days
	 * @var array
	 */
//	protected $_forecasts = null;
	
	protected $_dependancies = array();
	
	protected $_required = array(
		'url' => 'http://www.meteofrance.com/previsions-meteo-france/saint-clement-de-la-place/49370',
	);
	
	protected $_extracted = array();
	
	/**
	 *
	 * @var Dom
	 */
	protected $_dom = null;

	public function init($dependancies) {
		$this->_dependancies = $dependancies;
	}

	public function load() {
		$this->_dom = new Dom();
		$this->_dom->loadFromUrl($this->_dependancies['url']);

		// extract data
		$datas = $this->_extract();
		
		// prepare forecasts
		$this->_forecasts = array();
		foreach($datas as $data) {
			$this->addForecast($this->_prepare($data));
		}
	}

	/**
	 * Take direct array data from _extrac() and return a Forecast
	 * @param type $data
	 * @return \Romain\MeteoAlert\Source\Forecast
	 */
	protected function _prepare($data) {
		$forecast = new Forecast($data['period']);
		foreach($data as $name => $stat) {
			if($stat instanceof Stat\StatInterface) {
				$forecast->set($name, $stat);
			}
		}

		if(!empty($data['details'])) {
			foreach($data['details'] as $detail) {

				foreach($detail as $detail_name => $detail_value) {
					// is stat is worse (or not y et present in forecast)
					if($detail_value instanceof Stat\StatInterface) {
						$forecast->setIfWorse($detail_name, $detail_value);
					}
				}
			}
		}

		if(!empty($data['extra_details'])) {
			foreach($data['extra_details'] as $extra_detail_name => $extra_detail) {
				foreach($extra_detail as $extra_detail_value) {
					if($extra_detail_value instanceof Stat\StatInterface) {
						$forecast->setIfWorse($extra_detail_name, $extra_detail_value);
					}
				}
			}
		}
		
		return $forecast;
	}
	
	protected function _extract() {
		// tableau des prévisions par jour
		$forecasts = array();
		$blocks = $this->_dom->find('.bloc-day-summary');

		// pas besoin de plus de 3 (6 ?) jours
		$max = 3;
		$current = 0;
		foreach ($blocks as $content) {
			$current++;
			
			// a priori le premier jour (jour courant) ne nous intéresse pas vraiment
			// (mais à voir, il peut geler le soir)
			if($current == 1) {
//				continue;
			}

			// get the class attr
			$id = str_replace('day-symmary-id-', '', $content->getAttribute('id'));
			$forecasts[$id] = array(
				'summary' => new Stat\Summary(trim((string)$content->find('.day-summary-broad')->text())),
				'date' => $content->find('.day-summary-title')->text(),// jour concerné
				//'period' => $content->find('.day-summary-title')->text(),// jour concerné
				'period' => date('Y-m-d', time()+(($current-1) * 60 * 60 * 24)),
				'temperature-min' => new Stat\TemperatureMin($this->min($content->find('.day-summary-temperature')->text(true))),
				'wind-speed-max' => new Stat\WindSpeedMax($this->entier($content->find('.vent-detail-vitesse')->text())),
				// extra
				'details' => array()
			);
			
			/// on a les infos de plus haut niveau et les ids,
			//on va chercher les infos détaillées dans les sous blocs
			$details_divs = $this->_dom->find('#detail-day-symmary-id-'.$id.' .bloc-day-summary');
			if($details_divs && $details_divs instanceof \Traversable) {
				foreach($details_divs as $div) {
					$details = array(
						'period' => $div->find('.day-summary-title')->text(),
						'temperature-min' => new Stat\TemperatureMin($this->min($div->find('.day-summary-temperature')->text(true))),
						'wind-speed-max' => new Stat\WindSpeedMax($this->entier($div->find('.vent-detail-vitesse')->text())),
						'summary' => $div->find('.day-summary-broad')->text(),
						'frost-probability' => new Stat\FrostProbability($this->entier($div->find('.proba-gel')->text())),
					);
					$forecasts[$id]['details'][] = $details;
					
					// on vérifie qu'il n'y a pas d'info pire que dans le résumé principal
					foreach($details as $key => $val) {
						if($val !== null && array_key_exists($key, $forecasts[$id])) {
						}
					}
				}
			}
			
			// pour les premiers jours, on a des infos encore plus détaillées
			$more_info_trs = $this->_dom->find('#more-info-day-symmary-id-'.$id.' tr');
			$formated_details = array();
			$stats = array(
				'period',
				'summary',
				'wind-speed-max',
				'rain-probability',
				'frost-probability'
			);
			if($more_info_trs instanceof \Traversable) {
				
				$i = 0;
				foreach($more_info_trs as $tr) {
					if(preg_match('/row-title/', $tr->getAttribute('class'))) {
						continue;
					}
				
					$tds = array();
					foreach($tr->find('th') as $td) {
						$text = $td->text(true);
						$tds[] = $text;
						$formated_details[] = array('period' => $text);
					}
					foreach($tr->find('td') as $td) {
						// beurk
						$td_value = null;
						switch ($stats[$i]) {
							case 'period':
							case 'summary':
								$td_value = trim($td->text(true));
								break;
							case 'temperature-min':
								$td_value = new Stat\TemperatureMin($this->entier($td->text(true)));
								break;
							case 'temperature-max':
								$td_value = new Stat\TemperatureMax($this->entier($td->text(true)));
								break;
							case 'wind-speed-max':
								$td_value = new Stat\WindSpeedMax($this->max($td->text(true)));
								break;
							case 'frost-probability':
								$td_value = new Stat\FrostProbability($this->entier($td->text(true)));
								break;
							case 'rain-probability':
								$td_value = new Stat\RainProbability($this->entier($td->text(true)));
								break;
							default:
								continue;
						}
						$tds[] = $td_value;
					}
					$forecasts[$id]['extra_details'][$stats[$i]] = $tds;
					$i++;
				}
			}
			
			// securité
			if($current >= $max) {
				break;
			}
		}

		return $forecasts;
	}
}