<?php

namespace Romain\MeteoAlert\Source;

use Goutte\Client;

trait GoutteTrait {

	/**
	 *
	 * @var Goutte client
	 */
	protected $_client = null;
	
	public function client() {
		return $this->_client;
	}
	
	public function initClient() {
		$this->_client = new Client();
	}
	
	
	public function request($url, $method = 'GET') {
		$this->_client->request('GET', $url);
		return $this->_client->getResponse()->getContent();
	}
}