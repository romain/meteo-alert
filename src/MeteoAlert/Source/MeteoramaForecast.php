<?php

namespace Romain\MeteoAlert\Source;

use Romain\MeteoAlert\Stat;
use PHPHtmlParser\Dom;

/**
 * @Todo synthèse des infos en détail et extra détail pour récupérer les infos les "pires"
 * @TODO passage des dépendances
 */
class MeteoramaForecast extends Base implements \IteratorAggregate {
	use ForecastsTrait;
	
	protected $_dependancies = array();
	
	protected $_required = array(
		'url' => 'http://www.meteorama.fr/m%C3%A9t%C3%A9o-saint-cl%C3%A9ment-de-la-place.html?v=heure-par-heure',
	);
	
	protected $_extracted = array();
	
	/**
	 *
	 * @var Dom
	 */
	protected $_dom = null;
	
	public function init($dependancies) {
		$this->_dependancies = $dependancies;
		//$this->initClient();
	}

	public function load() {
		$this->_dom = new Dom();
		$this->_dom->loadFromUrl($this->_dependancies['url']);

		// extract data
		foreach($this->_extract() as $forecast) {
			$this->addForecast($forecast);
		}
	}
	
	protected function _extract() {
		// tableau des prévisions par jour
		$forecasts = array();
		
		// on récupère les labels h3 pour chaque jours
		$labels = array();
		$h3 = $this->_dom->find('#city-weather-detailed-hours > h3');
		$day = 0;
		foreach($h3 as $label) {
			//$labels[] = $label->text();
			$labels[] = date('Y-m-d', time()+($day * 60 * 60 * 24));
			$day++;
		}

		// maintenant on parcours les tables
		$tables = $this->_dom->find('#city-weather-detailed-hours table');
		$i = 0;
		foreach($tables as $table) {
			$label = $labels[$i];
			
			$forecast = new Forecast($label);
			
			// résumé

			// humidité
			foreach($table->find('tr td.relative-moist') as $value) {
				$value = $this->entier($value->text());
				$forecast->setIfWorse('humidity-max', new Stat\HumidityMax($value));
				$forecast->setIfWorse('humidity-min', new Stat\HumidityMin($value));
			}

			// cloudiness
			foreach($table->find('tr td.cloudiness') as $value) {
				$value = $this->entier($value->text());
				$forecast->setIfWorse('cloudiness-max', new Stat\CloudinessMax($value));
				$forecast->setIfWorse('cloudiness-min', new Stat\CloudinessMin($value));
			}

			// vitesse du vent
			foreach($table->find('tr td.wind-speed') as $value) {
				$value = $this->entier($value->text());
				$forecast->setIfWorse('wind-speed-max', new Stat\WindSpeedMax($value));
			}
			
			foreach($table->find('tr td.wind-direction div[0]') as $value) {
				$value = $value->getAttribute('class');
				$forecast->setIfWorse('wind-direction', new Stat\WindDirection($this->direction($value)));
			}

			// températures
			foreach($table->find('tr td.temp div') as $value) {
				$value = $value->text();
				$forecast->setIfWorse('temperature-min', new Stat\TemperatureMin($this->entier($value)));
				$forecast->setIfWorse('temperature-max', new Stat\TemperatureMax($this->entier($value)));
			}

			// températures
			$pluie = 0;
			foreach($table->find('tr td.precipitation') as $value) {
				$pluie += $this->float($value);
			}
			$forecast->setIfWorse('rainfall', new Stat\Rainfall($pluie));

			$i++;
			$forecasts[$label] = $forecast;
		}
		return $forecasts;
	}
	
	public function temperature($text) {
		if($text === null) {
			return null;
		}
		$splited = explode(' | ', trim(str_replace('°C', '', (string)$text)));
		if(count($splited) == 1) {
			return (int)$splited[0];
		} else if(count($splited) == 2) {
			return min((int)$splited[0], (int)$splited[1]);
		}
		return null;
	}

	public function direction($text) {
		$text = preg_replace('/^wind-[0-9]/', '', $text);
		$labels = array(
			'SW' => 'sud-ouest',
			'SE' => 'sud-est',
			'NE' => 'nord-est',
			'NW' => 'nord-ouest',
			'E' => 'est',
			'W' => 'ouest',
			'S' => 'sud',
			'N' => 'nord',
		);
		foreach($labels as $letter => $label) {
			$text = str_replace($letter, $label, $text);
		}
		return $text;
	}
		
	public function vent($text) {
		if($text === null) {
			return null;
		}
		return (int)trim(str_replace(array('Vent', 'km/h'), '', (string)$text));
	}
	
	public function min($text) {
		$entier = $this->entier($text, false);
		if($entier === null) {
			return null;
		}
		if(count($entier) <= 1) {
			return reset($entier);
		}
		return (int)call_user_func_array('min', $entier);
	}
	
	public function entier($text, $first = true) {
		if($text === null) {
			return null;
		}
		$matches = array();
		preg_match_all('/\d+/', $text, $matches);
		if(empty($matches[0])) {
			return null;
		}
		
		if($first) {
			return (int)reset($matches[0]);
		}
		
		return $matches[0];
	}

	public function float($text, $first = true) {
		if($text === null) {
			return null;
		}
		$matches = array();
		preg_match_all('/\d+(?:(?:,|\.)\d+)?/', $text, $matches);
		if(empty($matches[0])) {
			return null;
		}

		if($first) {
			return (float)str_replace(',', '.', reset($matches[0]));
		}

		return $matches[0];
	}
}