<?php

namespace Romain\MeteoAlert\Source;

use Romain\MeteoAlert\Tools;
/**
 * Sources base
 *
 * Concret classes *MUST* implements \Traversable (Iterator, IteratorAggregate...)
 */
abstract class Base implements \Romain\MeteoAlert\Source\SourceInterface, Tools\ToArrayInterface, \Countable/*, \Traversable*/ {

}