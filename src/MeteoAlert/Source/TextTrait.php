<?php

namespace Romain\MeteoAlert\Source;


trait TextTrait {

	public function temperature($text) {
		if($text === null) {
			return null;
		}
		$splited = explode(' | ', trim(str_replace('°C', '', (string)$text)));
		if(count($splited) == 1) {
			return (int)$splited[0];
		} else if(count($splited) == 2) {
			return min((int)$splited[0], (int)$splited[1]);
		}
		return null;
	}

	public function vent($text) {
		if($text === null) {
			return null;
		}
		return (int)trim(str_replace(array('Vent', 'km/h'), '', (string)$text));
	}

	public function min($text) {
		$entier = $this->entier($text, false);
		if($entier === null) {
			return null;
		}
		if(count($entier) <= 1) {
			return reset($entier);
		}
		return (int)call_user_func_array('min', $entier);
	}

	public function max($text) {
		$entier = $this->entier($text, false);
		if($entier === null) {
			return null;
		}
		if(count($entier) <= 1) {
			return reset($entier);
		}
		return (int)call_user_func_array('max', $entier);
	}

	public function entier($text, $first = true) {
		if($text === null) {
			return null;
		}
		$matches = array();
		preg_match_all('/\d+/', $text, $matches);
		if(empty($matches[0])) {
			return null;
		}

		if($first) {
			return (int)reset($matches[0]);
		}

		return $matches[0];
	}
}