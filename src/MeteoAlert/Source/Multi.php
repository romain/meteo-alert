<?php

namespace Romain\MeteoAlert\Source;

/**
 * Many sources
 */
use Romain\MeteoAlert\Tools;

/**
 * Sources base
 *
 * Concret classes *MUST* implements \Traversable (Iterator, IteratorAggregate...)
 */
class Multi extends Base implements \IteratorAggregate {
	use ForecastsTrait;
	
	/**
	 *
	 * @var array SourceInterface
	 */
	protected $_sources = array();

	public function init($dependancies) {
		foreach($dependancies['sources'] as $source) {
			$this->addSource($source['source'], $source['dependancies']);
		}
	}
	
	public function addSource(SourceInterface $source, $dependancies) {
		$source->init($dependancies);
		$this->_sources[] = $source;
	}

	public function load() {
		foreach($this->_sources as $source) {
			$source->load();
			foreach($source as $forecast) {
				if(!$this->available($forecast->period())) {
					$this->addForecast(new Forecast($forecast->period()));
				}
				$found = $this->getForecast($forecast->period());
				if($found) {
					foreach($forecast as $name => $stat) {
						$found->setIfWorse($name, $stat);
					}
				}
			}
		}
	}

}