<?php
namespace Romain\MeteoAlert\Alert;

/**
 * 
 */
trait ConfigureTrait  {
	
	protected $_options = array();
	
	public function configure(array $options) {
		$this->_options = $options + $this->defaultConfig();
	}
	
	/**
	 * @return array
	 */
	abstract function defaultConfig();
	
	public function reset() {
		$this->_options = array();
	}
	
	public function config() {
		return $this->_options;
	}
	
	public function getConf($key) {
		if(array_key_exists($key, $this->_options)) {
			return $this->_options[$key];
		}
		return null;
	}
	
	
	
}