<?php
namespace Romain\MeteoAlert\Alert;

/**
 * 
 */
trait MessageTrait  {
	
	protected $_messages = array();
	
	public function addMessage($messages) {
		$this->_messages = $messages;
	}
	
	public function resetMessages() {
		$this->_messages = array();
	}
	
	public function getMessages() {
		return $this->_messages;
	}
	
}