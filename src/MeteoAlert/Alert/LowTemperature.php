<?php
namespace Romain\MeteoAlert\Alert;

use Romain\MeteoAlert\Source\SourceInterface;
use Romain\MeteoAlert\Source\Forecast;
use Romain\MeteoAlert\Stat;

/**
 * 
 */
class LowTemperature implements AlertInterface {
	use MessageTrait, ConfigureTrait;
	
	public function defaultConfig() {
		return array(
			'temperature-min' => 3
		);
	}
	
	public function checkSource(SourceInterface $source) {
		foreach($source->getAll() as $forecast) {
			$this->checkForecast($forecast);
		}
	}
	
	public function checkForecast(Forecast $forecast) {		
		if(
			$forecast->available('temperature-min') &&
			$forecast->get('temperature-min')->worseThan(new Stat\TemperatureMin($this->getConf('temperature-min')))
		) {
			$this->addMessage('température minimale prévue pour '.$forecast->period().' : '.$forecast->get('temperature-min')->value());
		}
	}

	public function checkSources(array $sources) {
		foreach($sources as $source) {
			$this->check($source);
		}
	}
}