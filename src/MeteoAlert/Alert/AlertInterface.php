<?php
namespace Romain\MeteoAlert\Alert;

use Romain\MeteoAlert\Source\SourceInterface;

interface AlertInterface {
	
	/**
	 * 
	 * @param array $options
	 */
	public function configure(array $options);
	
	/**
	 * 
	 * @param array $sources
	 */
	public function checkSources(array $sources);
	
	/**
	 * @return string alert message if present
	 */
	public function getMessages();
	
}