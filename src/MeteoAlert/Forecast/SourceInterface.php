<?php

namespace Romain\MeteoAlert\Source;


interface SourceInterface {
	
	/**
	 * Init required dependancies
	 * @param type $dependancies
	 */
	public function init($dependancies);
	
	/**
	 * Load source datas
	 */
	public function load();
	
	/**
	 * Return all stats
	 */
	public function getAll();
	
	/**
	 * Get a particular stat
	 */
	public function get($stat);
	
	/**
	 * Check if source handle a stats
	 * @param type $stat
	 */
	public function handleStat($stat);
	
	/**
	 * Return all handled stats
	 */
	public function handle();
	
}

